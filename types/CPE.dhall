{-
   Copyright 2020 Sage Mitchell

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
-}

let CPE
    : Type → Type → Type → Type
    =   λ(l : Type)
      → λ(nt : Type)
      → λ(t : Type)
      →   ∀(CPE : Type → Type → Type → Type)
        → ∀ ( cpe
            : { empty : CPE l nt t
              , term : t → CPE l nt t
              , nonTerm : nt → CPE l nt t
              , seq : CPE l nt t → CPE l nt t → CPE l nt t
              , alt : CPE l nt t → CPE l nt t → CPE l nt t
              , rep : CPE l nt t → CPE l nt t
              , not : CPE l nt t → CPE l nt t
              , cap : { expr : CPE l nt t, label : l } → CPE l nt t
              , foldCap :
                    { expr1 : CPE l nt t, expr2 : CPE l nt t, label : l }
                  → CPE l nt t
              }
            )
        → CPE l nt t

in  CPE
