{-
   Copyright 2020 Sage Mitchell

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
-}

let RET
    : Type → Type → Type
    =   λ(l : Type)
      → λ(x : Type)
      →   ∀(RET : Type → Type → Type)
        → ∀ ( ret
            : { empty : RET l x
              , concat : RET l x → RET l x → RET l x
              , union : RET l x → RET l x → RET l x
              , repetition : RET l x → RET l x
              , label : l → List (RET l x) → RET l x
              , typeVar : x → RET l x
              }
            )
        → RET l x

in  RET
