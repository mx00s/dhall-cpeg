{-
   Copyright 2020 Sage Mitchell

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
-}

let types = ../types.dhall

let CPE = types.CPE

let CPEG = types.CPEG

let Tree = types.Tree

let RET = types.RET

let L = < Prod | Int >

let NT = < Prod | Val >

let T = < D0 | D1 | D2 | D3 | D4 | D5 | D6 | D7 | D8 | D9 | Star >

let grammar
    : CPEG L NT T
    = let rules
          : NT → CPE L NT T
          = let prod
                : CPE L NT T
                =   λ(CPE : Type → Type → Type → Type)
                  → λ ( cpe
                      : { empty : CPE L NT T
                        , term : T → CPE L NT T
                        , nonTerm : NT → CPE L NT T
                        , seq : CPE L NT T → CPE L NT T → CPE L NT T
                        , alt : CPE L NT T → CPE L NT T → CPE L NT T
                        , rep : CPE L NT T → CPE L NT T
                        , not : CPE L NT T → CPE L NT T
                        , cap : { expr : CPE L NT T, label : L } → CPE L NT T
                        , foldCap :
                              { expr1 : CPE L NT T
                              , expr2 : CPE L NT T
                              , label : L
                              }
                            → CPE L NT T
                        }
                      )
                  → cpe.foldCap
                      { expr1 = cpe.nonTerm NT.Val
                      , expr2 = cpe.seq (cpe.term T.Star) (cpe.nonTerm NT.Val)
                      , label = L.Int
                      }

            let val
                : CPE L NT T
                =   λ(CPE : Type → Type → Type → Type)
                  → λ ( cpe
                      : { empty : CPE L NT T
                        , term : T → CPE L NT T
                        , nonTerm : NT → CPE L NT T
                        , seq : CPE L NT T → CPE L NT T → CPE L NT T
                        , alt : CPE L NT T → CPE L NT T → CPE L NT T
                        , rep : CPE L NT T → CPE L NT T
                        , not : CPE L NT T → CPE L NT T
                        , cap : { expr : CPE L NT T, label : L } → CPE L NT T
                        , foldCap :
                              { expr1 : CPE L NT T
                              , expr2 : CPE L NT T
                              , label : L
                              }
                            → CPE L NT T
                        }
                      )
                  → cpe.cap
                      { expr =
                          cpe.alt
                            (cpe.term T.D0)
                            ( cpe.alt
                                (cpe.term T.D1)
                                ( cpe.alt
                                    (cpe.term T.D2)
                                    ( cpe.alt
                                        (cpe.term T.D3)
                                        ( cpe.alt
                                            (cpe.term T.D4)
                                            ( cpe.alt
                                                (cpe.term T.D5)
                                                ( cpe.alt
                                                    (cpe.term T.D6)
                                                    ( cpe.alt
                                                        (cpe.term T.D7)
                                                        ( cpe.alt
                                                            (cpe.term T.D8)
                                                            (cpe.term T.D9)
                                                        )
                                                    )
                                                )
                                            )
                                        )
                                    )
                                )
                            )
                      , label = L.Int
                      }

            in  λ(nonTerm : NT) → merge { Prod = prod, Val = val } nonTerm

      let start
          : CPE L NT T
          =   λ(CPE : Type → Type → Type → Type)
            → λ ( cpe
                : { empty : CPE L NT T
                  , term : T → CPE L NT T
                  , nonTerm : NT → CPE L NT T
                  , seq : CPE L NT T → CPE L NT T → CPE L NT T
                  , alt : CPE L NT T → CPE L NT T → CPE L NT T
                  , rep : CPE L NT T → CPE L NT T
                  , not : CPE L NT T → CPE L NT T
                  , cap : { expr : CPE L NT T, label : L } → CPE L NT T
                  , foldCap :
                        { expr1 : CPE L NT T, expr2 : CPE L NT T, label : L }
                      → CPE L NT T
                  }
                )
            → cpe.nonTerm NT.Prod

      in  { production = rules, startExpr = start }

in  grammar
