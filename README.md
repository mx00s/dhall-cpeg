dhall-cpeg
==========

Implements types from [CPEG: A Typed Tree Construction from Parsing
Expression Grammars with Regex-Like Captures][CPEG paper] in [Dhall].

A `CPE` is a parsing expression and serves as a building block for
`CPEG` grammars. They correspond, respectively, to the parse
expressions and parsing expression grammars (PEGs) originally
described by [Bryan Ford]. Both have been extended to support labeling
expressions, much like named capture groups in regular
expressions. Unlike normal parsing expressions, `CPE`s can also
represent left-recursion.

The paper describes how to generate typed parsers for `CPE`s and
`CPEG`s. Parser generation can't be expressed in Dhall because, for
example, parsing terms requires equality checks and character equality
isn't supported. However, a programming language with a Dhall library
could implement the parser generator. After deserializing a Dhall
`CPEG` expression, it could produce:

  1. a parser which maps a list of terminals to either a `Tree` or
     failure
  2. a `RET` type which constrains the set of `Tree`s the parser may
     return

Files in `./examples` demonstrate the following types from the paper:

  - [x] CPE
  - [x] CPEG
  - [ ] Tree
  - [ ] RET

[CPEG paper]: https://arxiv.org/pdf/1812.07429v1.pdf
[Dhall]: https://dhall-lang.org/
[Bryan Ford]: https://bford.info/packrat/
